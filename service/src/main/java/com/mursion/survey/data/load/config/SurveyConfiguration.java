package com.mursion.survey.data.load.config;

import com.google.common.collect.ImmutableList;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SurveyConfiguration {

  @Value("${qualtrics.connectionPool.max:100}")
  private int connectionPoolSize;

  @Value("${qualtrics.readTimeout:10000}")
  private int readTimeout;

  @Value("${qualtrics.connectionTimeout:10000}")
  private int connectionTimeout;

  @Value("${qualtrics.oauthUri}")
  private String qualtricsOauthUri;

  @Value("${qualtrics.clientId}")
  private String qualtricsClientId;

  @Value("${qualtrics.clientSecret}")
  private String qualtricsSecret;

  @Value("${qualtrics.grantType}")
  private String qualtricsGrantType;

  @Value("${qualtrics.scope}")
  private String qualtricsOauthScope;

  @Bean
  public RestTemplate surveyRestTemplate() {
    HttpClientBuilder httpClientBuilder = HttpClients.
        custom().setMaxConnTotal(connectionPoolSize)
        .setMaxConnPerRoute(connectionPoolSize)
        .useSystemProperties();
    HttpComponentsClientHttpRequestFactory httpRequestFactory =
        new HttpComponentsClientHttpRequestFactory(httpClientBuilder.build());
    httpRequestFactory.setConnectionRequestTimeout(connectionTimeout);
    httpRequestFactory.setReadTimeout(readTimeout);
    return new RestTemplate(httpRequestFactory);
  }

  @Bean
  public OAuth2RestTemplate qualtricsOauth2Operations() {
    ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
    resourceDetails.setAccessTokenUri(qualtricsOauthUri);
    resourceDetails.setClientId(qualtricsClientId);
    resourceDetails.setClientSecret(qualtricsSecret);
    resourceDetails.setGrantType(qualtricsGrantType);
    resourceDetails.setScope(ImmutableList.of(qualtricsOauthScope));
    DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
    return new OAuth2RestTemplate(resourceDetails, clientContext);
  }
}
