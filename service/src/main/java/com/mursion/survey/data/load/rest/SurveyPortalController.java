package com.mursion.survey.data.load.rest;


import com.mursion.survey.data.load.dto.in.SurveyDataLoadRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;
import com.mursion.survey.data.load.exception.FileExtractFailureException;
import com.mursion.survey.data.load.service.SurveyExportSchedulerService;
import com.mursion.survey.data.load.service.SurveyInitExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SurveyPortalController implements SurveyPortalServiceClient {

  private final SurveyInitExportService surveyInitExportService;

  private final SurveyExportSchedulerService surveyExportSchedulerService;

  @Autowired
  public SurveyPortalController(
          SurveyInitExportService surveyInitExportService,
          SurveyExportSchedulerService surveyExportSchedulerService
  ) {
    this.surveyInitExportService = surveyInitExportService;
    this.surveyExportSchedulerService = surveyExportSchedulerService;

  }

    @Override
    public QualtricsExportResponse initQualtricsSurveyExport(SurveyDataLoadRequest surveyResponse) throws FileExtractFailureException {
        return surveyInitExportService.initQualtricsSurveyExport(surveyResponse);
    }

    @Override
    public String storeSurveyResponse(SurveyDataLoadRequest surveyResponse) throws FileExtractFailureException {
        return surveyExportSchedulerService.saveResponseFromQualtrics(surveyResponse);
    }



}
