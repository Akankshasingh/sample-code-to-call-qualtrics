package com.mursion.survey.data.load.qualtrics;

import com.mursion.survey.data.load.dto.in.QualtricsExportRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;

import java.io.IOException;
import java.util.List;

public interface QualtricsInteractionService {

  QualtricsExportResponse initiateExportSurveyFileRequest(QualtricsExportRequest qualtricsExportRequest, String surveyId);

  QualtricsExportResponse getExportSurveyProgress(String surveyId, String progressId);

  List<Object> getExportSurveyFile(String surveyId, String fileId) throws IOException;
}
