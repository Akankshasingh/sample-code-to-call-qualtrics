package com.mursion.survey.data.load.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception { //NOSONAR
    //ignoring sonar check to white list uris
    // we don't need CSRF because our token is invulnerable
    http.csrf().disable(); //NOSONAR

    http.authorizeRequests()
        .antMatchers("/**").permitAll();

    // disable page caching
    http
        .headers()
        .cacheControl();
  }
}
