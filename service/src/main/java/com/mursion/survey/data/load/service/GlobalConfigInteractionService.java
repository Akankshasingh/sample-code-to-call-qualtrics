package com.mursion.survey.data.load.service;

import java.util.List;

public interface GlobalConfigInteractionService {
  List<String> getAllSurveyIds();
}
