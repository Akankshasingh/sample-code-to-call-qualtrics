package com.mursion.survey.data.load.service.impl;

import com.mursion.survey.data.load.dto.in.QualtricsExportRequest;
import com.mursion.survey.data.load.service.GlobalConfigInteractionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class GlobalConfigInteractionServiceImpl implements GlobalConfigInteractionService {

  //TODO - Move these to some common util file
  private static final String CONTENT_TYPE = "Content-Type";
  private static final String API_KEY = "api-key";

  private final RestTemplate surveyRestTemplate;

  @Autowired
  public GlobalConfigInteractionServiceImpl(RestTemplate surveyRestTemplate) {
    this.surveyRestTemplate = surveyRestTemplate;
  }

  @Value("${global.config.service.uri}")
  private String globalConfigServiceUri;

  @Override
  public List<String> getAllSurveyIds() {
    Map<String, String> uriVals = new HashMap<>();
    uriVals.put("configType", "SURVEY_LIST");
    String getGlobalConfigUri = buildUri(globalConfigServiceUri, uriVals);
    log.info("Get surveyIds from global config configuration: {}", globalConfigServiceUri);
    HttpEntity<QualtricsExportRequest> request = new HttpEntity<>(getHttpHeaders());
    return surveyRestTemplate.exchange(getGlobalConfigUri, HttpMethod.GET, request, List.class).getBody();
  }

  private String buildUri(String qualtricsExportEndpoint, Map<String, String> uriValues) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(qualtricsExportEndpoint);
    return uriComponentsBuilder.buildAndExpand(uriValues).toUriString();
  }

  private HttpHeaders getHttpHeaders() {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(CONTENT_TYPE, "application/json");
    httpHeaders.add(API_KEY, "survey_data_load");
    return httpHeaders;
  }
}
