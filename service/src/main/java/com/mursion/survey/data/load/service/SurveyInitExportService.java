package com.mursion.survey.data.load.service;

import com.mursion.survey.data.load.dto.in.SurveyDataLoadRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;
import com.mursion.survey.data.load.exception.FileExtractFailureException;

public interface SurveyInitExportService {
  QualtricsExportResponse initQualtricsSurveyExport(SurveyDataLoadRequest qualtricsExportRequest) throws FileExtractFailureException;
}
