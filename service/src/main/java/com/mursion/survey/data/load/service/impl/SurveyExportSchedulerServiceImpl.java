package com.mursion.survey.data.load.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mursion.survey.data.load.dao.QualtricsFileSummaryRepository;
import com.mursion.survey.data.load.dao.SurveyRepository;
import com.mursion.survey.data.load.dao.entity.QualtricsFileSummary;
import com.mursion.survey.data.load.dao.entity.SurveyResponse;
import com.mursion.survey.data.load.dto.in.SurveyDataLoadRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;
import com.mursion.survey.data.load.exception.FileExtractFailureException;
import com.mursion.survey.data.load.mapping.SurveyMapper;
import com.mursion.survey.data.load.qualtrics.QualtricsInteractionService;
import com.mursion.survey.data.load.service.GlobalConfigInteractionService;
import com.mursion.survey.data.load.service.SurveyExportSchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class SurveyExportSchedulerServiceImpl implements SurveyExportSchedulerService {

  private static final String IN_PROGRESS = "inProgress"; // MOVE to enum file for status
  private static final String COMPLETED = "Completed";
  private static final String FAILED = "Failed";


  private final SurveyRepository surveyRepository;
  private final QualtricsFileSummaryRepository qualtricsFileSummaryRepository;
  private final QualtricsInteractionService qualtricsInteractionService;
  private final GlobalConfigInteractionService globalConfigInteractionService;
  private final SurveyMapper surveyMapper;

  @Autowired
  public SurveyExportSchedulerServiceImpl(
          SurveyRepository surveyRepository,
          QualtricsFileSummaryRepository qualtricsFileSummaryRepository,
          QualtricsInteractionService qualtricsInteractionService,
          GlobalConfigInteractionService globalConfigInteractionService,
          SurveyMapper surveyMapper
  ) {
    this.surveyRepository = surveyRepository;
    this.qualtricsFileSummaryRepository = qualtricsFileSummaryRepository;
    this.qualtricsInteractionService = qualtricsInteractionService;
    this.globalConfigInteractionService = globalConfigInteractionService;
    this.surveyMapper = surveyMapper;
  }


  @Override
  @Scheduled(cron = "${survey.data.load.schedule}")
  /**
   * Query file_summary table with surveyID, fetch records inProgress
   * Call qualtrics inProgress endpoint to check if file generation completed
   * if it is completed, call qualtrics download endpoint fetch the JSON
   * Save the responses into the Survey responses table
   */
  public String saveResponseFromQualtrics(SurveyDataLoadRequest qualtricsExportRequest) throws FileExtractFailureException {
    try {
      log.info("Get All Survey Ids from global config service....");
      List<String> surveyIds = globalConfigInteractionService.getAllSurveyIds();
      //for each surveyId
      surveyIds.forEach(surveyId -> {
        List<Object[]> recordBySurveyIdAndProgress =
                qualtricsFileSummaryRepository.findBySurveyIdAndProgress(surveyId, IN_PROGRESS);
        if (recordBySurveyIdAndProgress != null) {
          log.info("Retrieved inProgress records of count:{} for surveyId:{}", recordBySurveyIdAndProgress.size(), surveyId);
          recordBySurveyIdAndProgress.forEach(this::extractTheRecordAndSaveResponse);
        }
      });
    } catch (Exception ex) {
      throw new FileExtractFailureException("Exception Occurred while extracting pending files", ex);
    }
    return COMPLETED;
  }

  private void extractTheRecordAndSaveResponse(Object[] recordArray) {
    try {
      QualtricsFileSummary qualtricsRequest = getQualtricsFileSummary(recordArray);
      if (qualtricsRequest.getFileDownloadAttempt() < 3) {
        log.info("Calling qualtrics export API to fetch fileId info for progressId:{}", qualtricsRequest.getProgressId());
        QualtricsExportResponse exportSurveyProgressRecord =
            qualtricsInteractionService.getExportSurveyProgress(qualtricsRequest.getSurveyId(),
                qualtricsRequest.getProgressId());

        if (exportSurveyProgressRecord != null &&
            exportSurveyProgressRecord.getResult().getPercentComplete().equals("100.0")) {
          log.info("Get File from Qualtrics for fileId:{}", exportSurveyProgressRecord.getResult().getFileId());
          parseTheSurveyFileAndSave(qualtricsRequest, exportSurveyProgressRecord);
        }
      } else {
        log.info("Exhausted number of Qualtrics export file for surveyId:{}", qualtricsRequest.getSurveyId());
        QualtricsFileSummary fileSummary = qualtricsRequest
                .toBuilder()
                .exportStatus(FAILED)
                .build();
        qualtricsFileSummaryRepository.save(fileSummary);
      }
    } catch (Exception ex) {
      log.error("Exception Occurred while extracting pending files!!! {}", ex);
    }
  }

  private void parseTheSurveyFileAndSave(
          QualtricsFileSummary qualtricsRequest,
          QualtricsExportResponse exportSurveyProgressRecord) throws IOException {

    List<Object> records = qualtricsInteractionService.getExportSurveyFile(
            qualtricsRequest.getSurveyId(),
            exportSurveyProgressRecord.getResult().getFileId());
    //Check if we need to modify the surveyId with other field
    //Check if other setters need to be set using a standard
    if (Objects.nonNull(records) && records.size() > 0) {
      List<SurveyResponse> responses = new ArrayList<>();
      ObjectMapper mapper = new ObjectMapper();
      records.forEach(record -> {
        try {
          responses.add(
                  surveyMapper.mapHashMapToSurveyResponse(
                          mapper.readValue(mapper.writeValueAsString(record), LinkedHashMap.class)));
        } catch (JsonProcessingException e) {
          throw new RuntimeException(e);
        }
      });
      log.info("Saving responses for surveyId: {} fileId: {} count:{}", qualtricsRequest.getSurveyId(),
              exportSurveyProgressRecord.getResult().getFileId(),
              responses.size());
      surveyRepository.saveAll(responses);
      updateFileSummary(qualtricsRequest, exportSurveyProgressRecord, COMPLETED);
    } else {
      updateFileSummary(qualtricsRequest, exportSurveyProgressRecord, IN_PROGRESS);
    }
  }

  private void updateFileSummary(
          QualtricsFileSummary qualtricsRequest,
          QualtricsExportResponse exportSurveyProgressRecord,
          String status) {
    qualtricsFileSummaryRepository.updateFileSummaryByProgressId(
            new BigDecimal(exportSurveyProgressRecord.getResult().getPercentComplete()).setScale(2),
            exportSurveyProgressRecord.getResult().getFileId(), (qualtricsRequest.getFileDownloadAttempt()) + 1, status,
            qualtricsRequest.getProgressId());
  }

  private QualtricsFileSummary getQualtricsFileSummary(Object[] recordArray) {
    return QualtricsFileSummary.builder()
            .surveyId(recordArray[0].toString())
            .progressId(recordArray[1].toString())
            .startDate(ZonedDateTime.ofInstant(Instant.ofEpochMilli(((Timestamp) recordArray[2]).getTime()), ZoneId.systemDefault()))
            .endDate(ZonedDateTime.ofInstant(Instant.ofEpochMilli(((Timestamp) recordArray[3]).getTime()), ZoneId.systemDefault()))
            .percentComplete(new BigDecimal(recordArray[4].toString()))
            .exportStatus(recordArray[5].toString())
            .fileId(Optional.ofNullable(recordArray[6]).toString())
            .fileDownloadAttempt(Integer.parseInt(recordArray[7].toString()))
            .build();
  }

}
