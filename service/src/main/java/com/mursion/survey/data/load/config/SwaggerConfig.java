package com.mursion.survey.data.load.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket api(@Value("${survey-data-load-service.version}") String version) {
    return new Docket(DocumentationType.SWAGGER_2).select()
        .apis(RequestHandlerSelectors.basePackage("com.mursion.survey.data.load")).paths(PathSelectors.any())
        .build().apiInfo(apiInfo(version));
  }

  ApiInfo apiInfo(String version) {
    return new ApiInfoBuilder().title("MURSION Survey Data Load Service")
        .description("MURSION Survey Data Load Service REST API\n" + "For internal server-server communication.")
        .version(version).contact(new Contact("Pradeep kumar", "", "parashara.pradeep@mursion.com")).build();
  }
}
