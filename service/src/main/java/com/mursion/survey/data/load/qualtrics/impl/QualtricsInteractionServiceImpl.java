package com.mursion.survey.data.load.qualtrics.impl;

import com.mursion.survey.data.load.dto.in.QualtricsExportRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;
import com.mursion.survey.data.load.qualtrics.QualtricsInteractionService;
import com.mursion.survey.data.load.util.FileSystemHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class QualtricsInteractionServiceImpl implements QualtricsInteractionService {

  private final RestTemplate surveyRestTemplate;

  private final OAuth2RestTemplate qualtricsRestTemplate;

  @Autowired
  public QualtricsInteractionServiceImpl(
          RestTemplate surveyRestTemplate,
          OAuth2RestTemplate qualtricsOauth2Operations
  ) {
    this.surveyRestTemplate = surveyRestTemplate;
    this.qualtricsRestTemplate = qualtricsOauth2Operations;
  }

  @Value("${qualtrics.export.init.uri}")
  private String qualtricsExportEndpoint;

  @Value("${qualtrics.export.progress.uri}")
  private String qualtricsProgressEndpoint;

  @Value("${qualtrics.export.download.uri}")
  private String qualtricsDownloadFileEndpoint;

  @Override
  public QualtricsExportResponse initiateExportSurveyFileRequest(QualtricsExportRequest qualtricsExportRequest, String surveyId) {
    Map<String, String> uriVals = new HashMap<>();
    uriVals.put("surveyId", surveyId);
    String surveyExportUrl = buildUri(qualtricsExportEndpoint, uriVals);
    log.info("Calling Init qualtrics export init surveyId:{} ...", surveyId);
    HttpEntity<QualtricsExportRequest> request =
            new HttpEntity<>(
                    qualtricsExportRequest,
                    getHttpHeaders(qualtricsRestTemplate.getAccessToken().getValue()));
    return surveyRestTemplate.exchange(surveyExportUrl, HttpMethod.POST, request, QualtricsExportResponse.class).getBody();
  }

  @Override
  public QualtricsExportResponse getExportSurveyProgress(String surveyId, String progressId) {
    Map<String, String> uriVals = new HashMap<>();
    uriVals.put("surveyId", surveyId);
    uriVals.put("progressId", progressId);
    String surveyExportUrl = buildUri(qualtricsProgressEndpoint, uriVals);
    log.info("Calling qualtrics export status surveyId:{}  progressId:{}...", surveyId, progressId);
    HttpEntity<QualtricsExportRequest> request =
            new HttpEntity<>(getHttpHeaders(qualtricsRestTemplate.getAccessToken().getValue()));
    return surveyRestTemplate.exchange(surveyExportUrl, HttpMethod.GET, request, QualtricsExportResponse.class).getBody();
  }

  @Override
  public List<Object> getExportSurveyFile(String surveyId, String fileId) throws IOException {
    Map<String, String> uriVars = new HashMap<>();
    uriVars.put("surveyId", surveyId);
    uriVars.put("fileId", fileId);
    String surveyExportUrl = buildUri(qualtricsDownloadFileEndpoint, uriVars);
    log.info("Calling qualtrics get export file surveyId:{}  fileId:{}...", surveyId, fileId);
    HttpEntity<QualtricsExportRequest> request =
        new HttpEntity<>(
                getHttpHeaders(qualtricsRestTemplate.getAccessToken().getValue(),
                MediaType.APPLICATION_OCTET_STREAM));
    byte[] arr = surveyRestTemplate.exchange(surveyExportUrl, HttpMethod.GET, request, byte[].class).getBody();
    return FileSystemHandler.csvToJsonFileConverter(arr);
  }

  private String buildUri(String qualtricsExportEndpoint, Map<String, String> uriValues) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(qualtricsExportEndpoint);
    return uriComponentsBuilder.buildAndExpand(uriValues).toUriString();
  }


  private HttpHeaders getHttpHeaders(String qualtricsToken) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add("Content-Type", "application/json");
    httpHeaders.add("Authorization", "Bearer " + qualtricsToken);
    return httpHeaders;
  }

  private HttpHeaders getHttpHeaders(String qualtricsToken, MediaType applicationOctetStream) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add("Authorization", "Bearer " + qualtricsToken);
    httpHeaders.setAccept(Collections.singletonList(applicationOctetStream));
    return httpHeaders;
  }
}
