package com.mursion.survey.data.load.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import liquibase.util.csv.opencsv.CSVReader;
import liquibase.util.csv.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

@Slf4j
@Component
public class FileSystemHandler {
  public static List<Object> csvToJsonFileConverter(byte[] csvInput) {
    List<Object> readAll = null;
    try {
      byte[] fileArray = writeToCSVFile(csvInput);
      fileArray = manipulateCsvFile(fileArray);

      CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
      CsvMapper csvMapper = new CsvMapper();
      // Read data from CSV file
      readAll = csvMapper.readerFor(Map.class).with(csvSchema).readValues(fileArray).readAll();
      ObjectMapper mapper = new ObjectMapper();
      mapper.writeValueAsString(readAll);
    } catch (IOException ex) {
      log.error("Exception occurred while extracting file {}", ex);
    }
    return readAll;
  }


  private static byte[] writeToCSVFile(byte[] csvInput) throws IOException {
    byte[] returnArr = null;
    CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
    CsvMapper csvMapper = new CsvMapper();
    // Read data from CSV file
    ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(csvInput));
    if (null != zipInputStream.getNextEntry()) { // NOSONAR
      //ignoring sonar cause null check is done
      List<Map> fileData = csvMapper.readerFor(Map.class).with(csvSchema).<Map>readValues(zipInputStream).readAll();
      zipInputStream.close();

      CsvSchema.Builder schemaBuilder = CsvSchema.builder();
      fileData.get(0).keySet().forEach(col -> {
        schemaBuilder.addColumn(col.toString());
      });

      // Create a File and append if it already exists.
      ByteArrayOutputStream csvBytes = new ByteArrayOutputStream();
      Writer writer = new OutputStreamWriter(csvBytes);
      CsvSchema schema = schemaBuilder.build().withLineSeparator(System.lineSeparator()).withHeader();
      CsvMapper mapper = new CsvMapper();
      mapper.writer(schema).writeValues(writer).writeAll(fileData);
      writer.flush();
      writer.close();
      returnArr = csvBytes.toByteArray();
    }
    return returnArr;
  }

  public static byte[] manipulateCsvFile(byte[] fileBytes) throws IOException {
    CSVReader reader = new CSVReader(
        new InputStreamReader(
            new ByteArrayInputStream(fileBytes)));
    List<String[]> allElements = reader.readAll();
    allElements.remove(0);
    allElements.remove(1);

    ByteArrayOutputStream csvBytes = new ByteArrayOutputStream();
    PrintWriter printWriter = new PrintWriter(csvBytes);
    CSVWriter csvWriter = new CSVWriter(printWriter);

    allElements.forEach(csvWriter::writeNext);

    csvWriter.close();
    printWriter.close();
    reader.close();
    return csvBytes.toByteArray();
  }
}
