package com.mursion.survey.data.load.rest;

import com.mursion.scheduler.server.common.BindingResultBuilder;
import com.mursion.scheduler.server.common.ErrorDTO;
import com.mursion.scheduler.server.common.ValidationDetailDTO;
import com.mursion.survey.data.load.exception.FileExtractFailureException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ControllerAdvice
@Slf4j
@RefreshScope
public class ErrorHandler extends ResponseEntityExceptionHandler {

  public static ErrorDTO toDTO(ConstraintViolationException ex) {
    List<ValidationDetailDTO> lst = new ArrayList<>(ex.getConstraintViolations().size());
    for (ConstraintViolation cv : ex.getConstraintViolations()) {
      lst.add(
          ValidationDetailDTO.builder()
              .objectName(BindingResultBuilder.getObjectName(cv.getRootBean()))
              .field(Optional.ofNullable(cv.getPropertyPath()).map(Object::toString).orElse(""))
              .rejectedValue(cv.getInvalidValue())
              .message(cv.getMessage())
              .build()
      );
    }
    return ErrorDTO.validateData(lst);
  }

  public static ErrorDTO toDTO(BindingResult bindingResult) {
    List<ValidationDetailDTO> lst = new ArrayList<>();

    for (ObjectError error : bindingResult.getAllErrors()) {
      ValidationDetailDTO.ValidationDetailDTOBuilder b = ValidationDetailDTO.builder()
          .message(error.getDefaultMessage())
          .objectName(error.getObjectName())
          .code(error.getCode());
      if (error instanceof FieldError) {
        FieldError fieldError = (FieldError) error;
        b
            .field(fieldError.getField())
            .rejectedValue(fieldError.getRejectedValue());
      }
      lst.add(b.build());
    }

    return ErrorDTO.validateData(lst);
  }

  @ExceptionHandler({ConstraintViolationException.class})
  public static ResponseEntity<ErrorDTO> handleUncaughtValidationException(ConstraintViolationException ex) {
    return handleErrorDTO(toDTO(ex), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({PersistenceException.class})
  public ResponseEntity<ErrorDTO> handleUncaughtValidationException(PersistenceException ex, WebRequest request) {
    if (ex.getCause() instanceof ConstraintViolationException) {
      return handleUncaughtValidationException((ConstraintViolationException) ex.getCause());
    }
    return handleOther(ex, request);
  }

  @ExceptionHandler({TransactionSystemException.class})
  public ResponseEntity<ErrorDTO> handleTransactionSystemException(TransactionSystemException ex, WebRequest request) {
    if (ex.getCause() instanceof PersistenceException) {
      return handleUncaughtValidationException((PersistenceException) ex.getCause(), request);
    }
    return handleOther(ex, request);
  }

  @ExceptionHandler({RuntimeException.class})
  public ResponseEntity<ErrorDTO> handleOther(RuntimeException ex, WebRequest request) {
    ErrorDTO dto = ErrorDTO.unknown();
    log.error("Unexpected exception\n{} {}\n{}",
        (request instanceof ServletWebRequest ? ((ServletWebRequest) request).getHttpMethod() : ""),
        request.getDescription(false),
        dto,
        ex);

    request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, RequestAttributes.SCOPE_REQUEST);

    return handleErrorDTO(dto, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler({FileExtractFailureException.class})
  public ResponseEntity<Object> handleInvalidRequestException(FileExtractFailureException ex) {
    ErrorDTO errorDTO = ErrorDTO.parseJson(ex.getMessage());
    log.error("Exception id: {}\nDetail Error: {}", errorDTO.getId(), ex.getMessage());
    return (ResponseEntity) handleErrorDTO(errorDTO, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status,
                                                                WebRequest request) {
    ErrorDTO errorDTO = ErrorDTO.parseJson("Json parsing exception encountered.");
    log.error("Exception id: {}\nDetail Error: {}", errorDTO.getId(), ex.getMessage());
    return (ResponseEntity) handleErrorDTO(errorDTO, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
                                                                WebRequest request) {
    return (ResponseEntity) handleValidationError(ex.getBindingResult());
  }

  @Override
  protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return (ResponseEntity) handleValidationError(ex.getBindingResult());
  }

  private static ResponseEntity<ErrorDTO> handleValidationError(BindingResult bindingResult) {
    return handleErrorDTO(toDTO(bindingResult), HttpStatus.BAD_REQUEST);
  }

  private static ResponseEntity<ErrorDTO> handleErrorDTO(ErrorDTO errorDTO, HttpStatus status) {
    return handleErrorDTO(errorDTO, status, new HttpHeaders());
  }

  // log exception ID for each exception
  // additional information can be found by trace id in ELK
  private static ResponseEntity<ErrorDTO> handleErrorDTO(ErrorDTO errorDTO, HttpStatus status, HttpHeaders headers) {
    log.error("Exception id: {}\nError code: {}\nProperties: {}", errorDTO.getId(), errorDTO.getCode(), errorDTO.getProperties());
    return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON).headers(headers).body(errorDTO);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status,
                                                           WebRequest request) {
    ErrorDTO dto = ErrorDTO.unknown();
    log.error("Unexpected exception\n" +
            "Method: {} {}\n" +
            "Body: {}\n" +
            "Exception id: {}",
        (request instanceof ServletWebRequest ? ((ServletWebRequest) request).getHttpMethod() : ""),
        request.getDescription(false),
        body,
        dto.getId(),
        ex);

    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, RequestAttributes.SCOPE_REQUEST);
    }

    return (ResponseEntity) handleErrorDTO(dto, status, headers);
  }
}
