package com.mursion.survey.data.load.service.impl;

import com.mursion.survey.data.load.dao.QualtricsFileSummaryRepository;
import com.mursion.survey.data.load.dao.entity.QualtricsFileSummary;
import com.mursion.survey.data.load.dto.in.QualtricsExportRequest;
import com.mursion.survey.data.load.dto.in.SurveyDataLoadRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;
import com.mursion.survey.data.load.exception.FileExtractFailureException;
import com.mursion.survey.data.load.qualtrics.QualtricsInteractionService;
import com.mursion.survey.data.load.service.GlobalConfigInteractionService;
import com.mursion.survey.data.load.service.SurveyInitExportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Slf4j
@Service
public class SurveyResponseServiceImpl implements SurveyInitExportService {

  private final QualtricsInteractionService qualtricsInteractionService;

  private final QualtricsFileSummaryRepository qualtricsFileSummaryRepository;

  private final GlobalConfigInteractionService globalConfigInteractionService;

  @Autowired
  public SurveyResponseServiceImpl(
          QualtricsInteractionService qualtricsInteractionService,
          QualtricsFileSummaryRepository qualtricsFileSummaryRepository,
          GlobalConfigInteractionService globalConfigInteractionService
  ) {
    this.qualtricsInteractionService = qualtricsInteractionService;
    this.qualtricsFileSummaryRepository = qualtricsFileSummaryRepository;
    this.globalConfigInteractionService = globalConfigInteractionService;
  }

  @Override
  public QualtricsExportResponse initQualtricsSurveyExport(SurveyDataLoadRequest surveyDataLoadRequest) throws FileExtractFailureException {
    QualtricsExportResponse response = null;
    try {
      List<String> surveyIds = globalConfigInteractionService.getAllSurveyIds();
      log.info("Retrieved surveyIds from global config {}", surveyIds);
      for (String surveyId : surveyIds) {
        log.info("Init survey init for surveyId: {}", surveyId);
        QualtricsExportRequest qualtricsExportRequest = getQualtricsExportRequest(surveyDataLoadRequest);
        response = qualtricsInteractionService.initiateExportSurveyFileRequest(qualtricsExportRequest, surveyId);
        if (null != response) {
          QualtricsFileSummary fileSummary = buildFileSummary(response, surveyDataLoadRequest, surveyId);
          qualtricsFileSummaryRepository.save(fileSummary);
        }
      }
    } catch (Exception e) {
      log.info("Failed to init export process {}", e.getMessage());
      throw new FileExtractFailureException("Failed to init export process", e);
    }
    return response;
  }

  private QualtricsExportRequest getQualtricsExportRequest(SurveyDataLoadRequest surveyDataLoadRequest) {
    return QualtricsExportRequest.builder()
            .startDate(surveyDataLoadRequest.getStartDate())
            .endDate(surveyDataLoadRequest.getEndDate())
            .format(surveyDataLoadRequest.getFormat())
            .timeZone(surveyDataLoadRequest.getTimeZone())
            .includeLabelColumns(false)
            .useLabels(true)
            .build();
  }

  private QualtricsFileSummary buildFileSummary(
          QualtricsExportResponse response,
          SurveyDataLoadRequest qualtricsExportRequest,
          String surveyId) {
    return QualtricsFileSummary.builder()
            .surveyId(surveyId)
            .startDate(ZonedDateTime.parse(qualtricsExportRequest.getStartDate()))
            .endDate(ZonedDateTime.parse(qualtricsExportRequest.getEndDate()))
            .exportStatus(response.getResult().getStatus())
            .progressId(response.getResult().getProgressId())
            .percentComplete(new BigDecimal(response.getResult().getPercentComplete()))
            .build();
  }
}
