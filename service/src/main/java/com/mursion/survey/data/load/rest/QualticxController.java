package com.mursion.survey.data.load.rest;

import com.jcraft.jsch.Logger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


@RestController
@Slf4j
public class QualticxController {
    public String accessToken = null;
    public String progressId = null;

    public String fileId = null;

    @PostMapping("/gettoken")
    public String getToken() {
        log.info("-------in the getToken---------");
        String clientId = "0fbf1dcbcf10b99acf6990a886eef57a";
        String clientSecret = "4qGtLAXaRxsy6BAz57WJzYFr7jnWy4hZ84JqGP6l7hxtRcXIFBDjRIFoCd46Besz";
        String dataCenter = "sjc1";

        String baseUrl = "https://sjc1.qualtrics.com/oauth2/token";
        String data = "{'grant_type': 'client_credentials','scope': 'manage:all'}";
        String SCOPE = "manage:all";
        OAuthClient client = new OAuthClient(new URLConnectionClient());
        OAuthClientRequest request = null;
        String token = null;
        try {
            request = OAuthClientRequest.tokenLocation(baseUrl)
                    .setGrantType(GrantType.CLIENT_CREDENTIALS)
                    .setClientId(clientId)
                    .setClientSecret(clientSecret)
                    .setScope(SCOPE)
                    .buildBodyMessage();
            try {
                token = client.accessToken(request,
                        OAuth.HttpMethod.POST,
                        OAuthJSONAccessTokenResponse.class).getAccessToken();
            } catch (OAuthProblemException e) {
                throw new RuntimeException(e);
            }

            System.out.println(token);
            log.info("----------token-------::" + token);


        } catch (OAuthSystemException e) {
            throw new RuntimeException(e);
        }
        accessToken = token;
        return token;

        // output: ed8f28f7-a30b-4bab-a6c1-6771108d43d0
        //in postman [Post ]: http://localhost:8100/gettoken
        //authrization: inherit auth
    }

    @PostMapping("/startExportResponse")
    public String startExportResponse() {
        String dataCenter = "sjc1";
        String surveyId = "SV_bwTq9jfj7qsSn30";
        String res = "";
        String url = "https://" + dataCenter + ".qualtrics.com/API/v3/surveys/" + surveyId + "/export-responses";
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = null;
        String BearerToken = "Bearer " + accessToken;
        try {
            HttpPost request = new HttpPost(url);
            StringEntity params = new StringEntity("{\"format\":\"csv\" } ");

            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", BearerToken);

            request.setEntity(params);
            response = httpClient.execute(request);
            res = EntityUtils.toString(response.getEntity());
            log.info("-------response code ::::: " + response.getStatusLine().getStatusCode());

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(res);
            JSONObject result = (JSONObject) json.get("result");
            progressId = (String) result.get("progressId");
            log.info("---progressId-----" + progressId);


        } catch (Exception ex) {
            // handle exception here
        }
        return res;
        //http://localhost:8100/startExportResponse
    }


    @GetMapping("/getExportResponseProgress")
    public String getExportResponseProgress() {
        String dataCenter = "sjc1";
        String surveyId = "SV_bwTq9jfj7qsSn30";
        String c = progressId;

        log.info("-----progressId------" + progressId);
        String url = "https://sjc1.qualtrics.com/API/v3/surveys/SV_bwTq9jfj7qsSn30/export-responses/" + progressId;

        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = null;
        String BearerToken = "Bearer " + accessToken;
        String res = null;
        try {
            HttpGet request = new HttpGet(url);
            request.setHeader("content-type", "application/json");
            request.setHeader("Authorization", BearerToken);

            response = httpClient.execute(request);
            res = EntityUtils.toString(response.getEntity());
            log.info("-------response code ::::" + response.getStatusLine().getStatusCode());
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(res);
            JSONObject result = (JSONObject) json.get("result");
            fileId = (String) result.get("fileId");
            log.info("-------file id ::::" + fileId);

        } catch (Exception ex) {
            // handle exception here
        }


        return res;

    }


    @GetMapping("/getExportResponseFile")
    public String getExportResponseFile() throws IOException {
        HttpResponse response = null;
        String res = null;
        String dataCenter = "sjc1";
        String surveyId = "SV_bwTq9jfj7qsSn30";
        String fieldId = fileId;
        String filename = "response.csv";
        Integer chunk_size = 100;

        String url = "https://sjc1.qualtrics.com/API/v3/surveys/" + surveyId + "/export-responses/" + fieldId + "/file";

        HttpClient httpClient = new DefaultHttpClient();
        String BearerToken = "Bearer " + accessToken;

        HttpGet request = new HttpGet(url);
        request.addHeader("content-type", "application/zip");
        request.addHeader("Authorization", BearerToken);
        response = httpClient.execute(request);
        log.info("-------response: code:::" + response.getStatusLine().getStatusCode());

        String EXTENSION = ".csv";
        String destDirectory = "D:/";
        InputStream inputStream = response.getEntity().getContent();
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(inputStream);
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdirs();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
        return "Ok | File saved in D drive";

    }



    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        int BUFFER_SIZE = 4096;
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }



}
