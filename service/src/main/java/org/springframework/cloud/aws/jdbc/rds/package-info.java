/**
 * Override some classes to fix issues with read replicas
 *
 * @author Alexey Nakidkin {@literal <aleksei_nakidkin@epam.com>}
 */

package org.springframework.cloud.aws.jdbc.rds;