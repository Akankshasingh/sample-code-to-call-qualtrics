#!/bin/bash

# creating bootstrap.yml
mkdir -p artifacts/config

cat > artifacts/config/bootstrap.yml << EOF
spring:
  cloud:
    config:
      uri:
EOF
for CFG in ${CFG_LAN}
do
  echo "        - $CFG" >> artifacts/config/bootstrap.yml
done
