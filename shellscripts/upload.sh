#!/bin/bash

#This script handles upload routine
echo "DEPLOYMENT ON $SERVERS STARTED"
i=0
for SERVER in ${SERVERS}
do
  echo "DEPLOYMENT ON $SERVER STARTED"
  ssh -p22 competentum@"$SERVER" "rm -rf ~/mrsn-$PROJECT_NAME/*.jar ~/mrsn-$PROJECT_NAME/config/*"
  ssh -p22 competentum@"$SERVER" "mkdir -p ~/mrsn-$PROJECT_NAME"
  scp -r artifacts/* competentum@"$SERVER":~/mrsn-$PROJECT_NAME
  ssh -p22 competentum@"$SERVER" "mkdir -p ~/datadog"
  scp -r datadog/* competentum@"$SERVER":~/datadog
  ssh -p22 competentum@"$SERVER" "echo spring.cloud.stream.instanceIndex=$((i++)) > ~/mrsn-$PROJECT_NAME/config/application.properties"
  echo $PROJECT_NAME uploaded to $SERVER

  ssh -p22 competentum@"$SERVER" "chmod +x ~/mrsn-$PROJECT_NAME/launch.sh"
  echo "DEPLOYMENT ON $SERVER COMPLETE"
done
echo "DEPLOYMENT ON $SERVERS COMPLETE"
