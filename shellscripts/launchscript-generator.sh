#!/bin/bash

# This script generates launchsripts

#This gets all the vars for launchscript unchanged
GETPIDANDJAR='`ps ax | grep java | grep ${APPNAME} | while read pid rest; do echo $pid; done`
[ -z "${PID}" ] || (kill -9 ${PID} && sleep 1)

JARFile=`ls *.jar`'
INFODIE='info() {
    echo "[DEBUG] $*" >&2
}

die() {
    echo "[ERROR] $*" >&2
    exit 1
}'
HOMEDIR='${APPHOME}'
VARNAME='${APPNAME}'

#Create folder structure and launchfiles
mkdir -p artifacts
cat > artifacts/launch.sh << EOF
#!/bin/sh
APPNAME=$PROJECT_NAME
APPHOME=/home/competentum/mrsn-$PROJECT_NAME

$INFODIE

cd $HOMEDIR || die "App home not found"

PID=$GETPIDANDJAR

echo > nohup.out
nohup java $JAR_PROPS -XX:+UseG1GC -XX:+UseStringDeduplication -XX:+HeapDumpOnOutOfMemoryError -jar \$JARFile 2> out.err 1> /dev/null < /dev/null & echo

info "Starting"

(timeout -k10s 10s tail -f out.err) || true

ps ax | grep java | grep ${VARNAME}

info "Finished"

exit 0
EOF

echo "$PROJECT_NAME script generated"
