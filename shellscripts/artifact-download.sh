#!/bin/bash

# This script downloads artifacts from artifactory.

groupId=$(mvn -Dexec.executable='echo' -Dexec.args='${project.groupId}' $MAVEN_CLI_OPTS --non-recursive exec:exec -q) #Get MVN groupId from POM file
echo $groupId
artifactId=$PROJECT_NAME
echo $artifactId
echo $CI_COMMIT_REF_NAME
echo $CI_COMMIT_TAG

if [[ $CI_COMMIT_TAG =~ survey-dataload-[0-9]+.[0-9]+.[0-9]+ ]]; then
  echo deploy release version
  version=${CI_COMMIT_TAG##*-}
  path=$MAVEN_REPO_RELEASE_URL/${groupId//.//}/$artifactId/$version
  build=$version
else
  echo deploy snapshot
  version=$(mvn -Dexec.executable='echo' -Dexec.args='${project.version}' $MAVEN_CLI_OPTS --non-recursive exec:exec -q) #Get MVN artifactId from POM file
  path=$MAVEN_REPO_SNAPSHOT_URL/${groupId//.//}/$artifactId/$version
  build=$(curl -H "X-JFrog-Art-Api:$ARTIFACTORY_KEY" -s $path/maven-metadata.xml | grep '<value>' | head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/")
fi

jar=$PROJECT_NAME-$build.jar
url=$path/$jar
echo $url
mkdir -p artifacts
rm -f artifacts/*.jar                                                                            # cleanup if exists
curl -H "X-JFrog-Art-Api:$ARTIFACTORY_KEY" -s "$url" -o artifacts/$jar && echo "$jar downloaded" # Download routine, checks should be added in future
apt install wget
mkdir -p datadog
wget -O datadog/dd-java-agent.jar 'https://dtdg.co/latest-java-tracer'
