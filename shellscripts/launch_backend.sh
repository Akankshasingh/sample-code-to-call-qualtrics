#!/bin/bash

# This is automated launchscript for the service on a remote env
for SERVER in ${SERVERS}
do
  echo "Launching $SERVER"
  ssh -p22 competentum@"$SERVER" "sudo /home/competentum/mrsn-$PROJECT_NAME/launch.sh"
  echo $PROJECT_NAME started on $SERVER
done
