.init: &init # Creation of unified build&deploy container steps
  - 'mvn --version'
  - 'java -version'
  - 'apt-get update -qq && apt-get install -y -qq openssh-client git curl'
  - 'mkdir -p ~/.ssh'
  - 'eval $(ssh-agent -s)'
  - 'echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config' # Needed to enable non-interactive SSH connection

.deploy_backend_template: &deploy_back # deploy backend typical routine, similar for all envs
  - 'ssh-add <(echo "$DEPLOY_KEY" | base64 -d)' #SSH key added to provide connection to servers
  - 'echo -e "section_start:`date +%s`:upload\r\e[0KDEPLOYMENT STARTED"'
  - 'bash shellscripts/appconfig-generator.sh' # Generate appconfigs
  - 'bash shellscripts/artifact-download.sh' # Download artifacts from artifactory
  - 'bash shellscripts/upload.sh'
  - 'echo -e "section_end:`date +%s`:upload\r\e[0KDEPLOYMENT DONE"'

.setup-git: &setup-git
  - 'apt-get update -qq && apt-get install -y -qq bash openssh-client git'
  - eval $(ssh-agent -s)
  - echo "${GITBOT_SSH_KEY}" | tr -d '\r' | ssh-add -
  - git config --global user.email "mrsn-gitbot"
  - git config --global user.name "Project Buildmaster"
  - mkdir -p ~/.ssh
  - chown 7000 ~/.ssh
  - ssh-keyscan gitlab.com > ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts

image: 'maven:3.8-openjdk-17-slim'

variables:
  MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
  GIT_RELEASE_TAG: survey-data-load
  PROJECT_NAME: "survey-data-load-service"
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  GIT_DEPTH: "0"

cache:
  paths:
    - .m2/repository/
    - .sonar/cache
  key: "${CI_JOB_NAME}"

stages:
  - sonar
  - build
  - deploy

sonarqube-check:
  tags:
    - be
  stage: sonar
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop"' # merge request for develop
    - if: '$CI_COMMIT_REF_NAME == "develop"' # Autorun this job only on develop
    - if: $CI_COMMIT_TAG
      when: never
    - when: manual
  script:
    - echo $SLACK_PAYLOAD > payload.json
    - chmod +x scripts/sonar_analysis.sh;./scripts/sonar_analysis.sh $SLACK_HOOK

maven-build:
  tags:
    - be
  stage: build
  rules:
    - if: '$CI_COMMIT_REF_NAME == "develop"' # Autorun this job only on develop
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, tags and release branches
      when: never
    - when: manual # All other cases are manually-run
  before_script:
    - *init
  script:
    - 'mvn $MAVEN_CLI_OPTS -T 1C deploy -DskipTests' # Tests pass in the Sonar Step
  artifacts:
    paths:
      - service/target/*.jar
    expire_in: 1h

deploy_sandbox1:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/ || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, temporal release tags and release branches
      when: never
    - when: manual
  variables:
    CFG_LAN: http://127.0.0.1:8888/spring
    SERVERS: ${SERVER_SCHEDULER_SANDBOX1}
    JAR_PROPS: -Xms64m -Xmx256m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5016
  dependencies: [ ]
  before_script:
    - *init # Create proper container for build
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"' # Launch services through proper launchscripts for envs - differs between envs same way as generator

deploy_sandbox2:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/ || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, temporal release tags and release branches
      when: never
    - when: manual
  variables:
    CFG_LAN: http://127.0.0.1:8888/spring
    SERVERS: ${SERVER_SCHEDULER_SANDBOX2}
    JAR_PROPS: -Xms64m -Xmx256m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5016
  dependencies: [ ]
  before_script:
    - *init
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"'

deploy_sandbox3:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/ || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, temporal release tags and release branches
      when: never
    - when: manual
  variables:
    CFG_LAN: http://127.0.0.1:8888/spring
    SERVERS: ${SERVER_SCHEDULER_SANDBOX3}
    JAR_PROPS: -Xms64m -Xmx256m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5016
  dependencies: [ ]
  before_script:
    - *init # Create proper container for build
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"' # Launch services through proper launchscripts for envs - differs between envs same way as generator

deploy_sandbox4:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/ || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, temporal release tags and release branches
      when: never
    - when: manual
  variables:
    CFG_LAN: http://127.0.0.1:8888/spring
    SERVERS: ${SERVER_SCHEDULER_SANDBOX4}
    JAR_PROPS: -Xms64m -Xmx256m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5016
  dependencies: [ ]
  before_script:
    - *init # Create proper container for build
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"' # Launch services through proper launchscripts for envs - differs between envs same way as generator

deploy_testing:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/ || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, temporal release tags and release branches
      when: never
    - when: manual
  variables:
    CFG_LAN: http://${SERVER_SCHEDULER_TESTING_2_LAN_IP}:8888/spring http://${SERVER_SCHEDULER_TESTING_4_LAN_IP}:8888/spring
    SERVERS: ${SERVER_SCHEDULER_TESTING_3}
    JAR_PROPS: -Xms128m -Xmx512m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5016
  dependencies: [ ]
  before_script:
    - *init
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"'

deploy_integration-testing:
  tags:
    - integration_be
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/ || $CI_COMMIT_REF_NAME =~ /^release-[0-9]+.[0-9].[0-9]$/' # Don't run and don't show this step on merge requests, temporal release tags and release branches
      when: never
    - when: manual
  variables:
    CFG_LAN: http://${SERVER_SCHEDULER_INTEGRATION_TESTING_2_LAN_IP}:8888/spring http://${SERVER_SCHEDULER_INTEGRATION_TESTING_4_LAN_IP}:8888/spring
    SERVERS: ${SERVER_SCHEDULER_INTEGRATION_TESTING_3_LAN_IP}
    JAR_PROPS: -Xms128m -Xmx512m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5016
  dependencies: [ ]
  before_script:
    - *init
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"'

deploy_staging:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /^survey-data-load-[0-9]+.[0-9].[0-9]$/'
      when: manual
  variables:
    CFG_LAN: http://${SERVER_SCHEDULER_STAGING_2_LAN_IP}:8888/spring http://${SERVER_SCHEDULER_STAGING_4_LAN_IP}:8888/spring
    SERVERS: ${SERVER_SCHEDULER_STAGING_3}
    JAR_PROPS: -Xms128m -Xmx512m
  dependencies: [ ]
  before_script:
    - *init
  script:
    - 'bash shellscripts/launchscript-generator.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"'

deploy_prod:
  tags:
    - be
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /^survey-data-load-[0-9]+.[0-9].[0-9]$/'
      when: manual
  variables:
    ENV: prod
    CFG_LAN: http://${SERVER_SCHEDULER_PROD_2_LAN_IP}:8888/spring http://${SERVER_SCHEDULER_PROD_4_LAN_IP}:8888/spring
    SERVERS: ${SERVER_SCHEDULER_PROD_3}
    JAR_PROPS: -Xms512m -Xmx1G
  dependencies: [ ]
  before_script:
    - *init
  script:
    - 'bash shellscripts/launchscript-generator-datadog.sh'
    - *deploy_back
    - 'bash shellscripts/launch_backend.sh && echo "SERVICES LAUNCHED"'

release:
  tags:
    - be
  stage: build
  rules:
    - if: '$CI_COMMIT_TAG =~ /^release-[0-9]+.[0-9].[0-9]$/'
  before_script:
    - *setup-git
  script:
    - 'echo tag=$CI_COMMIT_TAG'
    - 'releaseVersion=${CI_COMMIT_TAG#*-}'
    - 'echo releaseVersion=$releaseVersion'
    - 'git tag -l | xargs git tag -d'
    - 'git fetch --tags'
    - 'git branch -D $CI_COMMIT_TAG &>/dev/null || true'
    - 'git checkout tags/$CI_COMMIT_TAG -b "$CI_COMMIT_TAG"'
    #    - https://github.com/mojohaus/versions-maven-plugin/issues/336
    #    - https://github.com/mojohaus/versions-maven-plugin/issues/192
    - 'mvn $MAVEN_CLI_OPTS versions:update-properties -DallowIncrementalUpdates=false -DallowMajorUpdates=false -DallowMinorUpdates=false -DgenerateBackupPoms=false -Dincludes=\${project.groupId}:::: -DreleaseSnapshot=true'
    - 'mvn $MAVEN_CLI_OPTS versions:set -DnewVersion=$releaseVersion -DgenerateBackupPoms=false'
    - 'mvn $MAVEN_CLI_OPTS install enforcer:enforce -Drules=requireReleaseDeps -Denforcer.skip=false'
    - 'git remote set-url --push origin git@gitlab.com:$CI_PROJECT_PATH'
    - 'git diff && git commit -a -m "[ci skip] prepare release: $releaseVersion" && git push -u origin refs/heads/$CI_COMMIT_TAG'
    - 'git tag $GIT_RELEASE_TAG-$releaseVersion && git push origin $GIT_RELEASE_TAG-$releaseVersion'
    - 'mvn $MAVEN_CLI_OPTS deploy'
    - 'git tag --delete $CI_COMMIT_TAG'
    - 'git push --delete origin refs/tags/$CI_COMMIT_TAG'
