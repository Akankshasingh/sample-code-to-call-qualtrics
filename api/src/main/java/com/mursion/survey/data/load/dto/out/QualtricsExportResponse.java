package com.mursion.survey.data.load.dto.out;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.LinkedHashMap;

@Setter
@Getter
public class QualtricsExportResponse implements Serializable {
  private QualtricsExportInitResponseResult result;
  private LinkedHashMap meta;
}
