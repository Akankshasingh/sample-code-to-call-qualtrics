package com.mursion.survey.data.load.config;


import com.mursion.survey.data.load.config.rds.ReadDataSourcePoolMetricsAutoConfiguration;

import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alexey Nakidkin {@literal <aleksei_nakidkin@epam.com>}
 */
@Configuration
@AutoConfigureAfter({ReadDataSourcePoolMetricsAutoConfiguration.class})
public class ActuatorMetricsConfig {

  @Bean
  InitializingBean forcePrometheusPostProcessor(BeanPostProcessor meterRegistryPostProcessor, PrometheusMeterRegistry registry) {
    return () -> meterRegistryPostProcessor.postProcessAfterInitialization(registry, "");
  }

}