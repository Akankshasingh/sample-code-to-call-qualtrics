package com.mursion.survey.data.load.dao;

import com.mursion.survey.data.load.dao.entity.SurveyResponse;
import com.mursion.survey.data.load.util.CrudRepositoryBase;

import org.springframework.stereotype.Repository;

@Repository
public interface SurveyRepository extends CrudRepositoryBase<SurveyResponse> {

}
