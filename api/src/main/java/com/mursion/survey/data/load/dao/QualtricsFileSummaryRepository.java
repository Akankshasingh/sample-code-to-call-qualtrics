package com.mursion.survey.data.load.dao;

import com.mursion.survey.data.load.dao.entity.QualtricsFileSummary;
import com.mursion.survey.data.load.util.CrudRepositoryBase;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public interface QualtricsFileSummaryRepository extends CrudRepositoryBase<QualtricsFileSummary> {
  @Query(value = "SElECT * FROM qualtrics_survey_file_summary  WHERE survey_id=:surveyId AND export_status=:exportStatus", nativeQuery = true)
  List<Object[]> findBySurveyIdAndProgress(@Param("surveyId") String surveyId, @Param("exportStatus") String exportStatus);

  @Modifying
  @Query(value = "Update QualtricsFileSummary s set s.percentComplete=:percentComplete," +
      "s.fileId=:fileId, s.fileDownloadAttempt=:fileDownloadAttempt," +
      "s.exportStatus=:exportStatus where s.progressId=:progressId")
  void updateFileSummaryByProgressId(
          BigDecimal percentComplete,
          String fileId,
          int fileDownloadAttempt,
          String exportStatus,
          String progressId);
}
