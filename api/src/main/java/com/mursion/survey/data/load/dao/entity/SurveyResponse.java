package com.mursion.survey.data.load.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Getter
@Entity
@Table(name = "qualtrics_survey_responses")
public class SurveyResponse {

  @Column
  @Id
  private String surveyResponseId;

  private String surveyResponseType;

  private int surveyProgress;

  private int surveyDuration;

  private String surveyFinished;

  private String surveyStartDate;

  private String surveyEndDate;

  private String recipientLastName;

  private String recipientFirstName;

  private String externalReference;

  private String locationLongitude;

  private String locationLatitude;

  private String distributionChannel;

  private String userLanguage;

  private String contractId;

  private String licenseeId;

  private String licenseeName;

  private String projectName;

  private String sessionId;

  private String clientName;

  private String userId;

  private String scenarioId;

  private String source;

  @Type(type = "text")
  private String surveyResponseJson;

  private Date surveyRecordDate;

  private String surveyId;


}
