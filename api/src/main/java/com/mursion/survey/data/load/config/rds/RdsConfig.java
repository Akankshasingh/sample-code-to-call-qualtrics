package com.mursion.survey.data.load.config.rds;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.aws.jdbc.config.annotation.RdsInstanceConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author Alexey Nakidkin {@literal <aleksei_nakidkin@epam.com>}
 */
@Configuration
@Profile("rds")
public class RdsConfig {

  @Bean
  @ConfigurationProperties("spring.datasource.hikari")
  public RdsInstanceConfigurer instanceConfigurer() {
    return new HikariDataSourceFactory();
  }
}
