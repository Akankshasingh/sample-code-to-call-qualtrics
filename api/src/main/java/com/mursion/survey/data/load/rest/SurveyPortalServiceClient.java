package com.mursion.survey.data.load.rest;

import com.mursion.survey.data.load.dto.in.SurveyDataLoadRequest;
import com.mursion.survey.data.load.dto.out.QualtricsExportResponse;
import com.mursion.survey.data.load.exception.FileExtractFailureException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@RequestMapping("/survey/internal/rest")
public interface SurveyPortalServiceClient {

  @PostMapping(value = "/qualtrics/export/init", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Export Survey Response")
  @ApiResponses({@ApiResponse(code = 200, message = "Success"), @ApiResponse(code = 400, message = "Bad request"),
      @ApiResponse(code = 403, message = "User does not have access")})
  QualtricsExportResponse initQualtricsSurveyExport(@RequestBody SurveyDataLoadRequest surveyResponse) throws FileExtractFailureException;

  @PostMapping(value = "/qualtrics/export/file", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Store Survey Response")
  @ApiResponses({@ApiResponse(code = 200, message = "Success"), @ApiResponse(code = 400, message = "Bad request"),
      @ApiResponse(code = 403, message = "User does not have access")})
  String storeSurveyResponse(@RequestBody SurveyDataLoadRequest surveyResponse) throws IOException, FileExtractFailureException;
}
