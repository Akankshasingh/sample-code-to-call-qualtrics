package com.mursion.survey.data.load.dto.out;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QualtricsExportInitResponseResult {

  private String progressId;
  private String percentComplete;
  private String status;
  private String fileId;
}
