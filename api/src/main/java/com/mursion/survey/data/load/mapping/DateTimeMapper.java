package com.mursion.survey.data.load.mapping;

import org.mapstruct.Mapper;

import java.util.Date;

@Mapper(componentModel = "spring")
public interface DateTimeMapper {

  default Long dateToTimestamp(Date date) {
    return date != null ? date.getTime() : null;
  }

  default Date timestampToDate(Long timestamp) {
    return timestamp != null ? new Date(timestamp) : null;
  }
}
