package com.mursion.survey.data.load.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Getter
@Entity
@Table(name = "qualtrics_survey_file_summary")
@IdClass(QualtricsFileSummary.class)
public class QualtricsFileSummary implements Serializable {

  @Id
  private String surveyId;
  @Id
  private String progressId;

  private ZonedDateTime startDate;
  private ZonedDateTime endDate;
  private BigDecimal percentComplete;
  private String exportStatus;
  private String fileId;
  private int fileDownloadAttempt;

}
