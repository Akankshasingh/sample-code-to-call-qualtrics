package com.mursion.survey.data.load.util;


import org.hibernate.ObjectNotFoundException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@NoRepositoryBean
public interface CrudRepositoryBase<T> extends CrudRepository<T, String> {

  default String getEntityName() {
    for (Class iface : getClass().getInterfaces()) {
      if (iface.getName().startsWith("com.mursion.scheduler") && iface.getSimpleName().endsWith("Repository")) {
        String name = iface.getSimpleName();
        return name.substring(0, name.length() - "Repository".length());
      }
    }
    throw new UnsupportedOperationException("Method getEntityName must be implemented manually");
  }

  /**
   * Get object from DB
   *
   * @param id object ID
   * @return entity
   * @throws ObjectNotFoundException if the object doesn't exist
   */
  default @Nonnull
  T getObject(String id) throws ObjectNotFoundException {
    Optional<T> op = findById(id);
    if (op.isPresent()) {
      return op.get();
    }
    throw new ObjectNotFoundException(id, getEntityName());
  }

  /**
   * Warning! Archived items are also returner by this method.
   */
  @Override
  Collection<T> findAllById(Iterable<String> ids);

  default Collection<T> findUnarchivedById(Iterable<String> ids) {
    return findAllById(ids).stream()
        .filter(e -> !(e instanceof Archivable) || !((Archivable) e).isArchived())
        .collect(Collectors.toList());
  }
}
