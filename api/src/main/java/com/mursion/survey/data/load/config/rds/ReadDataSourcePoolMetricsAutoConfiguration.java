package com.mursion.survey.data.load.config.rds;

import com.mursion.survey.data.load.config.ActuatorMetricsConfig;

import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.metrics.micrometer.MicrometerMetricsTrackerFactory;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
import org.springframework.boot.actuate.metrics.jdbc.DataSourcePoolMetrics;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceUnwrapper;
import org.springframework.boot.jdbc.metadata.DataSourcePoolMetadataProvider;
import org.springframework.cloud.aws.jdbc.datasource.ReadOnlyRoutingDataSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DelegatingDataSource;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Extended version of the {@link org.springframework.boot.actuate.autoconfigure.metrics.jdbc.DataSourcePoolMetricsAutoConfiguration} for read-only data sources.
 * The main difference is the {@link #routedDataSources(DataSource)} pre-processor
 *
 * @author Alexey Nakidkin {@literal <aleksei_nakidkin@epam.com>}
 * @see org.springframework.boot.actuate.autoconfigure.metrics.jdbc.DataSourcePoolMetricsAutoConfiguration
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureAfter({MetricsAutoConfiguration.class, DataSourceAutoConfiguration.class,
    SimpleMetricsExportAutoConfiguration.class})
@ConditionalOnClass({DataSource.class, MeterRegistry.class})
@ConditionalOnBean({DataSource.class, MeterRegistry.class})
@AutoConfigureBefore({ActuatorMetricsConfig.class})
public class ReadDataSourcePoolMetricsAutoConfiguration {

  @Configuration(proxyBeanMethods = false)
  @ConditionalOnBean(DataSourcePoolMetadataProvider.class)
  static class ReadDataSourcePoolMetadataMetricsConfiguration {

    private static final String DATASOURCE_SUFFIX = "dataSource";

    @Autowired
    void bindDataSourcesToRegistry(Map<String, DataSource> dataSources, MeterRegistry registry,
                                   ObjectProvider<DataSourcePoolMetadataProvider> metadataProviders) {
      List<DataSourcePoolMetadataProvider> metadataProvidersList = metadataProviders.stream()
          .collect(Collectors.toList());
      dataSources.forEach(
          (name, dataSource) -> routedDataSources(dataSource)
              .forEach(hikariDataSource -> bindDataSourceToRegistry(name, hikariDataSource, metadataProvidersList, registry)));
    }

    private void bindDataSourceToRegistry(String beanName, DataSource dataSource,
                                          Collection<DataSourcePoolMetadataProvider> metadataProviders, MeterRegistry registry) {
      String dataSourceName = getDataSourceName(beanName);
      new DataSourcePoolMetrics(dataSource, metadataProviders, dataSourceName, Collections.emptyList())
          .bindTo(registry);
    }

    /**
     * Get the name of a DataSource based on its {@code beanName}.
     *
     * @param beanName the name of the data source bean
     * @return a name for the given data source
     */
    private String getDataSourceName(String beanName) {
      if (beanName.length() > DATASOURCE_SUFFIX.length()
          && StringUtils.endsWithIgnoreCase(beanName, DATASOURCE_SUFFIX)) {
        return beanName.substring(0, beanName.length() - DATASOURCE_SUFFIX.length());
      }
      return beanName;
    }

  }

  @Slf4j
  @Configuration(proxyBeanMethods = false)
  @ConditionalOnClass(HikariDataSource.class)
  static class ReadHikariDataSourceMetricsConfiguration {

    private final MeterRegistry registry;

    ReadHikariDataSourceMetricsConfiguration(MeterRegistry registry) {
      this.registry = registry;
    }

    @Autowired
    void bindMetricsRegistryToHikariDataSources(Collection<DataSource> dataSources) {
      dataSources.stream()
          .map(ReadDataSourcePoolMetricsAutoConfiguration::routedDataSources)
          .flatMap(Collection::stream)
          .forEach(this::bindMetricsRegistryToHikariDataSource);
    }

    private void bindMetricsRegistryToHikariDataSource(HikariDataSource hikari) {
      if (hikari.getMetricRegistry() == null && hikari.getMetricsTrackerFactory() == null) {
        try {
          hikari.setMetricsTrackerFactory(new MicrometerMetricsTrackerFactory(this.registry));
        } catch (Exception ex) {
          log.warn("Failed to bind Hikari metrics: {}", ex.getMessage());
        }
      }
    }

  }

  private static List<HikariDataSource> routedDataSources(DataSource dataSource) {
    if ((dataSource instanceof DelegatingDataSource)) {
      dataSource = ((DelegatingDataSource) dataSource).getTargetDataSource();
    }
    if (!(dataSource instanceof ReadOnlyRoutingDataSource)) {
      return Collections.emptyList();
    }
    return ((ReadOnlyRoutingDataSource) dataSource).getDataSources().stream()
        .map(ds -> DataSourceUnwrapper.unwrap((DataSource) ds, HikariDataSource.class))
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

}
