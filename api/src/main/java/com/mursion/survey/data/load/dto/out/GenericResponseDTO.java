package com.mursion.survey.data.load.dto.out;

import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class GenericResponseDTO {
  private String message;
  private int code;
  private boolean success;
  private String detailMessage;
  private Object data;

  private static final int SUCCEED = 200;
  private static final int FAILURE = 500;

  public GenericResponseDTO success(String message, Object data) {
    return success(message, null, data);
  }

  public GenericResponseDTO success(String message, String detailMessage, Object data) {
    this.code = SUCCEED;
    this.success = true;
    this.message = message;
    this.detailMessage = detailMessage;
    this.data = data;
    return this;
  }

  public byte[] failure(String message) {
    return failure(message, null);
  }

  public byte[] failure(String message, String detailMessage) {
    this.code = FAILURE;
    this.success = false;
    this.message = message;
    this.detailMessage = detailMessage;
    return this.message.getBytes(StandardCharsets.UTF_8);
  }
}
