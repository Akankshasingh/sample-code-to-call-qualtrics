package com.mursion.survey.data.load.util;

public interface Archivable {

  boolean isArchived();

  default boolean isCascadeArchived() {
    return false;
  }

  default void setCascadeArchived(boolean cascade) {
  }

  void archive();

  void unarchive();
}
