package com.mursion.survey.data.load.config.rds;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.aws.jdbc.config.annotation.RdsInstanceConfigurer;
import org.springframework.cloud.aws.jdbc.datasource.DataSourceFactory;
import org.springframework.cloud.aws.jdbc.datasource.DataSourceInformation;
import org.springframework.cloud.aws.jdbc.datasource.support.DatabasePlatformSupport;
import org.springframework.cloud.aws.jdbc.datasource.support.StaticDatabasePlatformSupport;

import javax.sql.DataSource;

/**
 * A HikariCP {@link DataSourceFactory} implementation that creates a JDBC pool backed datasource.
 * Allows the configuration of all configuration properties except username, password, JDBC url and driver class name
 * because they are passed in while actually creating the datasource. All other properties can be modified by calling the respective setter methods.
 * This class uses a {@link DatabasePlatformSupport} implementation to actually retrieve the driver class name and url in order to create the datasource.
 * <p>All properties are derived from {@link HikariConfig}</p>
 * Inspired by {@link org.springframework.cloud.aws.jdbc.datasource.TomcatJdbcDataSourceFactory}
 *
 * @author Alexey Nakidkin {@literal <aleksei_nakidkin@epam.com>}
 */
public class HikariDataSourceFactory extends HikariConfig implements DataSourceFactory, RdsInstanceConfigurer {

  @Setter
  private DatabasePlatformSupport databasePlatformSupport = new StaticDatabasePlatformSupport();

  @Override
  public void setUsername(String username) {
    throw new UnsupportedOperationException("Username will be set at runtime!");
  }

  @Override
  public void setPassword(String password) {
    throw new UnsupportedOperationException("Password will be set at runtime!");
  }

  @Override
  public void setDriverClassName(String driverClassName) {
    throw new UnsupportedOperationException("Will be set at runtime!");
  }

  @Override
  public void setJdbcUrl(String url) {
    throw new UnsupportedOperationException("Will be set at runtime!");
  }

  @Override
  public HikariDataSource createDataSource(DataSourceInformation dataSourceInformation) {
    // create a method scoped instance
    HikariConfig configurationToUse = new HikariConfig();

    // copy all general properties
    BeanUtils.copyProperties(this, configurationToUse,
        "driverClassName", "exceptionOverrideClassName", "maximumPoolSize", "minimumIdle"
    );

    configurationToUse.setDriverClassName(this.databasePlatformSupport
        .getDriverClassNameForDatabase(dataSourceInformation.getDatabaseType()));
    configurationToUse.setJdbcUrl(this.databasePlatformSupport.getDatabaseUrlForDatabase(
        dataSourceInformation.getDatabaseType(),
        dataSourceInformation.getHostName(), dataSourceInformation.getPort(),
        dataSourceInformation.getDatabaseName()));
    configurationToUse.setUsername(dataSourceInformation.getUserName());
    configurationToUse.setPassword(dataSourceInformation.getPassword());

    if (StringUtils.isNotBlank(getExceptionOverrideClassName())) {
      configurationToUse.setExceptionOverrideClassName(getExceptionOverrideClassName());
    }
    if (getMaximumPoolSize() > 0) {
      configurationToUse.setMaximumPoolSize(getMaximumPoolSize());
    }
    if (getMinimumIdle() >= 0) {
      configurationToUse.setMinimumIdle(getMinimumIdle());
    }

    return new HikariDataSource(configurationToUse);
  }

  @Override
  public void closeDataSource(DataSource dataSource) {
    if (dataSource instanceof HikariDataSource) {
      ((HikariDataSource) dataSource).close();
    }
  }

  @Override
  public DataSourceFactory getDataSourceFactory() {
    return this;
  }
}
