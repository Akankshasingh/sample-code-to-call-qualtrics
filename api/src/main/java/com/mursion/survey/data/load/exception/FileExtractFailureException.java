package com.mursion.survey.data.load.exception;

public class FileExtractFailureException extends Exception {

  private String message;
  private Throwable cause;

  public FileExtractFailureException(String message) {
    super(message);
    this.message = message;
  }

  public FileExtractFailureException(String message, Throwable cause) {
    super(cause);
    this.message = message;
    this.cause = cause;
  }
}
