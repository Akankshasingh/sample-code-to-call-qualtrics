package com.mursion.survey.data.load.dto.in;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class QualtricsExportRequest {

  private String startDate;
  private String endDate;
  private String format;
  private String timeZone;
  private boolean includeLabelColumns;
  private boolean useLabels;

}
