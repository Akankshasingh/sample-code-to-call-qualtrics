package com.mursion.survey.data.load.hibernate;

import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.spi.MetadataBuilderContributor;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

public class CustomMetadataBuilderContributor implements MetadataBuilderContributor {

  @Override
  public void contribute(MetadataBuilder metadataBuilder) {
    // timestampDiffSec(startDate, endDate) -> distance in seconds
    metadataBuilder.applySqlFunction(
        "timestampDiffSec",
        new StandardSQLFunction(
            "timestampDiffSec",
            StandardBasicTypes.LONG
        )
    );

    // timestampAddSec(startDate, distance in seconds) -> endDate
    metadataBuilder.applySqlFunction(
        "timestampAddSec",
        new StandardSQLFunction(
            "timestampAddSec",
            StandardBasicTypes.TIMESTAMP
        )
    );

    // greatest(value1, value2, value3, ...) -> max of these values
    metadataBuilder.applySqlFunction(
        "greatest",
        new StandardSQLFunction(
            "greatest"
        )
    );

    // least(value1, value2, value3, ...) -> min of these values
    metadataBuilder.applySqlFunction(
        "least",
        new StandardSQLFunction(
            "least"
        )
    );
  }

}
