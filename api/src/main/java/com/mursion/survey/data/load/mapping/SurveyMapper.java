package com.mursion.survey.data.load.mapping;

import com.mursion.survey.data.load.dao.entity.SurveyResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.LinkedHashMap;


@Mapper(componentModel = "spring", uses = DateTimeMapper.class, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface SurveyMapper {

  default SurveyResponse mapHashMapToSurveyResponse(LinkedHashMap<String, String> surveyResponseObj) throws JsonProcessingException {
    return SurveyResponse.builder()
            .surveyStartDate(surveyResponseObj.get("Start Date"))
            .surveyEndDate(surveyResponseObj.get("End Date"))
            .surveyFinished(surveyResponseObj.get("Finished"))
            .surveyResponseId(surveyResponseObj.get("Response ID"))
            .surveyProgress(Integer.parseInt(surveyResponseObj.get("Progress")))
            .contractId(surveyResponseObj.get("sowId"))
            .licenseeId(surveyResponseObj.get("providerId"))
            .licenseeName(surveyResponseObj.get("providerName"))
            .clientName(surveyResponseObj.get("clientName"))
            .sessionId(surveyResponseObj.get("sessionId"))
            .scenarioId(surveyResponseObj.get("scenarioId"))
            .source(surveyResponseObj.get("source"))
            .userId(surveyResponseObj.get("userId"))
            .surveyId(surveyResponseObj.get("surveyId"))
            .surveyResponseJson(new ObjectMapper().writeValueAsString(surveyResponseObj))
            .build();
  }
}
