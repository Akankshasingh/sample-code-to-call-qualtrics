#! /bin/bash
mvn $MAVEN_CLI_OPTS verify sonar:sonar -Dsonar.qualitygate.wait=true -Dsonar.projectKey=com.mursion.scheduler:survey-data-load > sonar.log

if [ $? != 0 ]; then
    cat sonar.log
    echo "Failed"
    cat  sonar.log| grep -io 'https://sq-new.mursion.com/[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]' > sonar_url.txt
    echo "sonarurl"
    cat sonar_url.txt
    sed "s,https://sq-new.mursion.com/dashboard?id=com.mursion.survey,`cat sonar_url.txt`,g" payload.json >scripts/payload_old.json
    cat scripts/payload_old.json
    sed 's,"https://sq-new.mursion.com/dashboard?id=com.mursion.survey,",g' scripts/payload_old.json >scripts/payload_new.json
    sed "s,branch,\&branch,g" scripts/payload_new.json >scripts/payload.json
    echo "payload_new"
    cd scripts
    cat scrpits/payload.json
    echo "payload_new"
    cat payload.json
    curl -X POST -H 'Content-type: application/json' --data-binary "@payload.json" $1
    exit 1
else
    cat sonar.log
fi
